// canigetanapp

let App = require('chaturbate').app;
let Goals = require('./plugins/goals');
let Tickets = require('./plugins/tickets');

let app = new App(cb);
app.register(new Goals());
app.register(new Tickets());
app.run();
