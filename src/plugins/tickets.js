
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Tickets extends Plugins {
    constructor() {
        super();
        this.name = 'Ticket show';

        this.settings = [
            {
                type: 'choice',
                name: 'tickets_enable',
                label: 'Enable or disable ticket show on launch',
                choice1: 'Disable',
                choice2: 'Enable',
                defaultValue: 'Disable'
            },
            {
                type: 'int',
                name: 'tickets_interval',
                label: 'Display interval of notice (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 3
            },
            {
                type: 'int',
                name: 'tickets_price',
                label: 'Ticket price',
                minValue: 0,
                maxValue: 100000,
                defaultValue: 250
            },
            {
                type: 'int',
                name: 'tickets_fan_price',
                label: 'Ticket price for fanclub (0 to disable)',
                minValue: 0,
                maxValue: 100000,
                defaultValue: 0,
                required: false
            },
            {
                type: 'int',
                name: 'tickets_timer',
                label: 'Ticket sales timer (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 100000,
                defaultValue: 0,
                required: false
            },
            {
                type: 'str',
                name: 'tickets_name',
                label: 'Ticket show name',
                defaultValue: '',
                required: false
            },
            {
                type: 'str',
                name: 'tickets_notice_color',
                label: 'Ticket sales notice text color (hex)',
                defaultValue: Constants.colors.dark_green
            },
            {
                type: 'str',
                name: 'tickets_notice_bg',
                label: 'Ticket sales notice background color (hex)',
                defaultValue: Constants.colors.white
            },
            {
                type: 'str',
                name: 'tickets_color',
                label: 'Ticket show welcome text color (hex)',
                defaultValue: Constants.colors.dark_green
            },
            {
                type: 'str',
                name: 'tickets_bg',
                label: 'Ticket show welcome background color (hex)',
                defaultValue: Constants.colors.white
            }
        ];

        this.commands = {
            tickets: {
                access: ['host', 'mod'],
                handler: this.tickets,
                description: 'Do tickets stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Start ticket sales with "/tickets on", with optional price "/tickets on 250" or time "/tickets on 250 15"',
                        handler: this.enable,
                        success: 'Ticket sales started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Stop ticket sales with "/tickets stop" or "/tickets off"',
                        handler: this.disable,
                        success: 'Ticket sales stopped.'
                    },
                    {
                        names: ['name', 'edit'],
                        description: 'Edit ticket show name with "/tickets name New show name"',
                        handler: this.updateName,
                        success: 'Ticket show name updated.'
                    },
                    {
                        names: ['price'],
                        description: 'Edit ticket price with "/tickets price 250"',
                        handler: this.updatePrice,
                        success: 'Ticket show price updated.'
                    },
                    {
                        names: ['timer'],
                        description: 'Edit ticket show timer with "/tickets timer 15"',
                        handler: this.updateTimer,
                        success: 'Ticket show timer updated.'
                    },
                    {
                        names: ['show'],
                        description: 'Start or stop the ticket show with "/tickets show start" and "/tickets show stop"',
                        handler: this.toggleShow,
                        success: 'Ticket show status updated.'
                    },
                    {
                        names: ['sales', 'sale'],
                        description: 'Display "Ticket sales is over" message with "/tickets sales stop", hide with "/tickets sales start"',
                        handler: this.toggleSales,
                        success: 'Ticket sales message updated.'
                    },
                    {
                        names: ['add'],
                        description: 'Add a user to the ticket show with "/tickets add username"',
                        handler: this.addUser,
                        success: 'User added.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove a user from the ticket show with "/tickets remove username"',
                        handler: this.removeUser,
                        success: 'User removed.'
                    },
                    {
                        names: ['clear'],
                        description: 'Remove all users from the ticket show with "/tickets clear"',
                        handler: this.removeAllUsers,
                        success: 'All users removed.'
                    },
                    {
                        names: ['list'],
                        description: 'List ticket holders with "/tickets list"',
                        handler: this.listHolders,
                        success: 'Ticket holders listed.'
                    },
                    {
                        names: ['legends'],
                        description: 'Add legends to the ticket show with "/tickets legends"',
                        handler: this.addLegends,
                        success: 'Legends added.'
                    },
                    {
                        names: ['interval'],
                        description: 'Set notice interval in minutes with "/tickets interval 3"',
                        handler: this.setInterval,
                        success: 'Ticket sales notice interval set.'
                    }
                ]
            }
        };

        Object.assign(this.commands, {
            ticket: {
                access: ['host', 'mod'],
                handler: this.tickets,
                description: 'Alias for /tickets',
                params: /(.+)/,
                subcommands: this.commands.tickets.subcommands
            },
            timer: {
                access: ['host', 'mod'],
                handler: this.timer,
                description: 'Alias for /tickets timer',
                params: /(.+)/
            },
            show: {
                access: ['host', 'mod'],
                handler: this.show,
                description: 'Alias for /tickets show',
                params: /(.+)/
            },
            add: {
                access: ['host', 'mod'],
                handler: this.add,
                description: 'Alias for /tickets add',
                params: /(.+)/
            }
        });

        this.totals = {};
        this.since_started = {};
        // this.ticket_holders = [];

        this.show_name = '';
        this.show_price = 0;
        this.fan_price = 0;
        this.time_left = 0;
        this.over = false;
        this.elapsed = '';
        this.started = null;
        this.timer = 0;

        this.enabled = false;
        this.interval = 3 * 60000;

        this.legends_minimum = 1000;
    }

    enable(user, params) {
        let price = this.show_price;
        let time = this.time_left;

        if (params.length > 0)
            price = Number.parseInt(params[0], 10);
        if (Number.isNaN(price))
            price = this.show_price;
        else
            params.shift();

        if (params.length > 0)
            time = Number.parseInt(params[0], 10);
        if (Number.isNaN(time))
            time = this.time_left;
        else
            params.shift();

        if (params.length > 0)
            this.show_name = params.join(' ');

        this.time_left = time ? time + 1 : 0;
        this.show_price = price;
        this.enabled = true;
        this.over = false;

        this.addLegends();

        if (this.time_left > 0)
            this.timerInterval();

        this.api.drawPanel();
    }

    disable(user) {
        this.time_left = 0;
        this.show_name = '';
        this.enabled = false;
        if (this.api.limitCam_isRunning()) {
            this.api.sendNotice(
                'Warning! Ticket show is still running, stop the ticket show with "/ticket show stop"',
                user.name,
                Constants.colors.light_yellow
            );
        }
        if (this.timer)
            this.api.cancelTimeout(this.timer);
        this.api.drawPanel();
    }

    setInterval(user, params) {
        this.interval = Number.parseInt(params.join(''), 10) * 60000;
    }

    noticeInterval() {
        if (!this.enabled || this.show_price === 0 || this.interval === 0)
            return;
        this.showNotice();
        this.api.setTimeout(function(){ this.noticeInterval(); }.bind(this), this.interval);
    }

    showNotice(user) {
        let to = user ? user.name : '';
        this.api.sendNotice(
            `Ticket show${this.show_name ? `: ${this.show_name}` : ''} for ${this.show_price} tokens`,
            to,
            this.api.settings.tickets_notice_bg,
            this.api.settings.tickets_notice_color,
            'bold');
    }

    updateName(user, params) {
        this.show_name = params.join(' ');
        if (this.enabled)
            this.api.drawPanel();
    }

    updatePrice(user, params) {
        this.show_price = Number.parseInt(params, 10);
        if (this.enabled)
            this.api.drawPanel();
    }

    updateTimer(user, params) {
        let time = Number.parseInt(params.join(''), 10);
        this.time_left = time ? time + 1 : 0;
        if (this.enabled && this.time_left === 0)
            this.api.drawPanel();
        this.timerInterval();
    }

    timerInterval() {
        if (!this.enabled || !this.show_price || !this.time_left)
            return;
        this.time_left -= 1;

        // TODO setting to auto start show
        if (this.time_left === 0) {
            this.api.sendNotice(
                Constants.chars.text_hourglass + ' Ticket show is about to start!' + Constants.chars.text_hourglass,
                '',
                Constants.colors.white,
                Constants.colors.dark_red,
                'bold'
            );
        } else if (this.time_left <= 5) {
            this.api.sendNotice(
                Constants.chars.text_hourglass_flowing + ' There is ' + this.time_left +
                    ' minute' + (this.time_left > 1 ? 's' : '') + ' left to the timer!',
                '',
                Constants.colors.white,
                Constants.colors.dark_orange,
                'bold'
            );
        }

        this.api.drawPanel();
        if (this.timer)
            this.api.cancelTimeout(this.timer);
        this.timer = this.api.setTimeout(function(){ this.timerInterval(); }.bind(this), 60000);
    }

    elapsedInterval() {
        if (!this.enabled || this.started === null)
            return;
        this.setElapsed();
        this.api.drawPanel();
        this.api.setTimeout(function(){ this.elapsedInterval(); }.bind(this), 5000);
    }

    setElapsed() {
        if (this.started !== null) {
            let elapsed = new Date(Date.now() - this.started);
            let hours = elapsed.getUTCHours();
            let minutes = elapsed.getUTCMinutes();
            let seconds = elapsed.getUTCSeconds();
            if (seconds < 10)
                seconds = '0' + seconds;
            if (hours > 0 && minutes < 10)
                minutes = '0' + minutes;
            this.elapsed = (hours > 0 ? hours + ':' : '') + minutes + ':' + seconds;
        }
    }

    listHolders(user) {
        this.api.sendNotice(
            this.api.limitCam_allUsersWithAccess().join(', '),
            user.name,
            Constants.colors.light_pink);
    }

    toggleShow(user, params) {
        switch (params.join('').trim()) {
          case 'start':
          case 'on':
            if (!this.enabled)
                this.enable(user, [this.show_price, 0, this.show_name]);
            this.api.limitCam_start(
                `Ticket show is starting! There is still time to buy your ticket for ${this.show_price} tokens`);
            this.started = Date.now();
            this.elapsedInterval();
            this.time_left = 0;
            if (this.timer)
                this.api.cancelTimeout(this.timer);
            this.api.drawPanel();
            break;
          case 'stop':
          case 'off':
            this.api.limitCam_stop();
            this.setElapsed();
            this.disable(user);
            if (this.started !== null)
                this.started = null;
            this.since_started = {};
            break;
        }
    }

    toggleSales(user, params) {
        switch (params.join('').trim()) {
          case 'over':
          case 'stop':
          case 'off':
            this.over = true;
            break;
          default:
            this.over = false;
        }
        this.api.drawPanel();
    }

    addLegends() {
        let legends = [];
        for (let name of Object.keys(this.totals))
            if (this.totals[name] >= this.legends_minimum && !this.api.limitCam_userHasAccess(name)) {
                legends.push(name);
                this.api.sendNotice(
                    `Welcome to the ticket show, legendary ${name}! (sent privately)`,
                    name,
                    this.api.settings.tickets_bg,
                    this.api.settings.tickets_color,
                    'bold');
            }
        this.api.limitCam_addUsers(legends);
    }

    addUser(user, params) {
        this.api.limitCam_addUsers(params);
    }

    removeUser(user, params) {
        this.api.limitCam_removeUsers(params);
    }

    removeAllUsers(user, params) {
        this.api.limitCam_removeAllUsers();
    }

    onStart(user) {
        this.fan_price = Number.parseInt(this.api.settings.tickets_fan_price, 10);

        if (this.api.settings.tickets_enable === 'Enable') {
            this.enable(user, [this.api.settings.tickets_price,
                               this.api.settings.tickets_timer,
                               this.api.settings.tickets_name]);
        } else {
            this.show_price = Number.parseInt(this.api.settings.tickets_price, 10);
            this.time_left = Number.parseInt(this.api.settings.tickets_timer, 10);
            this.show_name = this.api.settings.tickets_name;
        }

        this.interval = Number.parseInt(this.api.settings.tickets_interval, 10) * 60000;
        if (this.interval > 0)
            this.noticeInterval();

        this.api.log('Tickets started.');
    }

    onTip(from, to, amount, message) {
        let last = 0;
        if (this.totals.hasOwnProperty(from.name)) {
            last = this.totals[from.name];
            this.totals[from.name] += amount;
        } else
            Object.assign(this.totals, {[from.name]: amount});

        if (!this.enabled || this.show_price === 0 || this.api.limitCam_userHasAccess(from.name))
            return;

        let since_started = 0;
        if (this.since_started.hasOwnProperty(from.name)) {
            since_started = this.since_started[from.name];
            this.since_started[from.name] += amount;
        } else
            Object.assign(this.since_started, {[from.name]: amount});

        if ((since_started < this.show_price && this.since_started[from.name] >= this.show_price) ||
            (this.fan_price && from.isFan && since_started < this.fan_price && this.since_started[from.name] >= this.fan_price) ||
            (last < this.legends_minimum && this.totals[from.name] >= this.legends_minimum)) {
            this.api.limitCam_addUsers([from.name]);
            if (!this.over)
                this.api.sendNotice(
                    `Welcome to the ticket show, ${from.name}!${from.isAnon ? ' (sent privately)' : ''}`,
                    (from.isAnon ? from.name : ''),
                    this.api.settings.tickets_bg,
                    this.api.settings.tickets_color,
                    'bold');
            else
                this.api.sendNotice(
                    'You were added to the show, however, please keep in mind ' +
                    'that ticket sales had ended earlier. (sent privately)',
                    from.name,
                    Constants.colors.yellow,
                    Constants.colors.black,
                    'bold');
        }
    }

    onEnter(user) {
        if (!this.enabled || this.current === '' || this.current_total === 0)
            return;
        this.showNotice(user);
    }

    onMessage(user, message) {
        if (!this.enabled)
            return;
        if (this.api.limitCam_userHasAccess(user.name))
            message.setBackground(Constants.colors.light_pink);
    }

    getStatus() {
        return `${this.name}: ${this.api.limitCam_isRunning() ? 'SHOW' : 'SALES'} ${this.enabled ? '[ running ]' : '[ stopped ]'}` +
               ` Notice every ${this.interval / 60000} minute${this.interval > 1 ? 's' : ''} - Available commands:`;
    }

    timer(user, params) {
        params = params.trim().split(' ');
        this.updateTimer(user, params);
        this.api.sendNotice('Timer updated', user.name, Constants.colors.light_green);
    }

    show(user, params) {
        params = params.trim().split(' ');
        this.toggleShow(user, params);
        this.api.sendNotice('Show status updated', user.name, Constants.colors.light_green);
    }

    add(user, params) {
        params = params.trim().split(' ');
        this.addUser(user, params);
        this.api.sendNotice('User added', user.name, Constants.colors.light_green);
    }

    onDrawPanel(user, panel) {
        if (this.enabled) {
            let show_name = 'Hidden show';
            let show_price = (this.show_price && !this.over) ? 'Tip ' + this.show_price + ' to watch' : 'Ticket sales is over';
            let time = this.time_left ? 'Time left: ' + this.time_left + ' minute' + (this.time_left > 1 ? 's' : '') : '';

            if (this.show_name)
                show_name += ': ' + this.show_name;

            if (this.api.limitCam_isRunning()) {
                show_name += ' (in progress)';
                time = 'Elapsed: ' + this.elapsed;
            }

            panel.setPanel({
                template: '3_rows_11_21_31',
                row1_value: show_name,
                row2_value: show_price,
                row3_value: time
            });
            return panel.getResponse();
        }
    }
}

module.exports = Tickets;
