
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Blocker extends Plugins {
    constructor() {
        super();
        this.name = 'Blocker';

        this.settings = [
            {
                type: 'choice',
                name: 'blocker_enable',
                label: 'Enable or disable blocking',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            },
            {
                type: 'str',
                name: 'blocker_words',
                label: 'Blocked words (lowercase)',
                defaultValue: 'cunt,bitch,slut,c2c,cumslut,whore'
            },
        ];

        this.commands = {
            block: {
                access: ['host', 'mod'],
                handler: this.block,
                description: 'Do blocking stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Enable word blocking with "/block start" or "/block on"',
                        handler: this.enable,
                        success: 'Word blocking started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Disable word blocking with "/block stop" or "/block off"',
                        handler: this.disable,
                        success: 'Word blocking stopped.'
                    },
                    {
                        names: ['reset'],
                        description: 'Reset blocked words to default settings with "/block reset"',
                        handler: this.initBlocker,
                        success: 'Blocked words reset to default settings.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'List blocked words with "/block list"',
                        handler: this.showWords,
                        success: 'Blocked words listed.'
                    },
                    {
                        names: ['add', 'new'],
                        description: 'Add a blocked word with "/block add badword"',
                        handler: this.addWord,
                        success: 'Blocked word added.'
                    },
                    {
                        names: ['edit', 'change'],
                        description: 'Edit a blocked word with "/block edit 3 newbadword"',
                        handler: this.editWord,
                        success: 'Blocked word updated.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove blocked word with "/block remove 3"',
                        handler: this.removeWord,
                        success: 'Blocked word removed.'
                    }
                ]
            }
        };

        this.mods = [];
        this.words = [];
        this.enabled = true;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    initBlocker() {
        if (!this.api.settings.blocker_words)
            return;
        let words = [];
        for (let word of this.api.settings.blocker_words.split(','))
            words.push(word.trim());
        this.words = words;
    }

    showWords(user) {
        let words = [];
        for (let i = 0; i < this.words.length; i++)
            words.push('#' + (i + 1) + ' - ' + this.words[i]);

        this.api.sendNotice(
            words.join('\n'),
            user.name,
            Constants.colors.white,
            Constants.colors.dark_red,
            'bold');
    }

    addWord(user, params) {
        let word = params.join(' ').trim();

        if (this.words.includes(word))
            return 'This word already exists.';

        this.words.push(word);
    }

    editWord(user, params) {
        let index = Number.parseInt(params.shift(), 10);
        let word = params.join(' ');

        if (index > this.words.length)
            return `Word #${index} does not exist.`;

        this.words[index - 1] = word;
    }

    removeWord(user, params) {
        let index = Number.parseInt(params, 10);

        if (index > this.words.length)
            return `Word #${index} does not exist.`;

        this.words.splice(index - 1, 1);
    }

    onStart() {
        this.initBlocker();

        if (this.api.settings.blocker_enable !== 'Enable')
            this.enabled = false;
    }

    onEnter(user) {
        if (user.isMod && !this.mods.includes(user.name)) {
            this.mods.push(user.name);
            this.api.sendNotice(
                `You've been added to the word blocker mods list. Shh.`,
                user.name,
                Constants.colors.light_orange,
                Constants.colors.dark_red,
                'bold');
        }
    }

    onMessage(user, message) {
        if (!this.enabled || user.hasPermission(['host', 'mod', 'fan']))
            return;

        for (let word of this.words) {
            if (message.message.length < word.length)
                continue;
            if (message.message.toLowerCase().includes(word)) {
                message.hide();
                let msg = `Blocked message from ${user.name}: ${message.message}`;
                this.api.sendNotice(
                    msg,
                    this.api.room_slug,
                    Constants.colors.light_orange,
                    Constants.colors.dark_red,
                    'bold');
                if (this.mods.length > 0)
                    for (let mod of this.mods)
                        this.api.sendNotice(
                            msg,
                            mod,
                            Constants.colors.light_orange,
                            Constants.colors.dark_red,
                            'bold');
                break;
            }
        }
    }

    getStatus() {
        return `${this.name}: ${ this.enabled ? '[ running ]' : '[ stopped ]'} - Available commands:`;
    }
}

module.exports = Blocker;
