
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Whispers extends Plugins {
    constructor() {
        super();
        this.name = 'Whispers';

        this.settings = [
            {
                type: 'choice',
                name: 'whispers_enable',
                label: 'Enable or disable whispers',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            }
        ];

        this.commands = {
            whispers: {
                access: ['host', 'mod'],
                handler: this.whispers,
                description: 'Do whispers stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Enable whispers with "/whispers start" or "/whispers on"',
                        handler: this.enable,
                        success: 'Whispers started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Disable whispers with "/whispers stop" or "/whispers off"',
                        handler: this.disable,
                        success: 'Whispers stopped.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'List whisperers with "/whispers list"',
                        handler: this.showUsers,
                        success: 'Whisperers listed.'
                    },
                    {
                        names: ['add', 'new'],
                        description: 'Add a whisperer with "/whispers add username"',
                        handler: this.addUser,
                        success: 'Whisperer added.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove a whisperer with "/whispers remove username"',
                        handler: this.removeUser,
                        success: 'Whisperer removed.'
                    }
                ]
            },
            w: {
                access: ['host', 'mod'],
                handler: this.whisperAll,
                description: 'Whisper to mods and broadcaster',
                params: /(.+)/
            },
            wb: {
                access: ['host', 'mod'],
                handler: this.whisperHost,
                description: 'Whisper to broadcaster only',
                params: /(.+)/
            },
            wm: {
                access: ['host', 'mod'],
                handler: this.whisperMods,
                description: 'Whisper to mods only',
                params: /(.+)/
            }
        };

        this.mods = [];
        this.enabled = true;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    showUsers(user) {
        this.api.sendNotice(
            'Whisperers: ' + this.mods.join(', '),
            user.name,
            Constants.colors.white,
            Constants.colors.dark_red,
            'bold');
    }

    addUser(user, params) {
        let whisperer = params.join('').trim();

        if (!this.mods.includes(whisperer)) {
            this.mods.push(whisperer);
            this.api.sendNotice(
                `You've been added to the whisperers list. Shh.`,
                whisperer,
                Constants.colors.dark_grey,
                Constants.colors.light_grey,
                'bold');
        } else if (user.name !== whisperer)
            return `${whisperer} already added.`;
    }

    removeUser(user, params) {
        let whisperer = params.join('').trim();
        let index = this.mods.indexOf(whisperer);
        if (index < 0)
            return `${whisperer} is not amongst whisperers.`;
        this.mods.splice(index, 1);
    }

    onStart() {
        if (this.api.settings.whispers_enable !== 'Enable')
            this.enabled = false;
    }

    onEnter(user) {
        if (user.isMod && !this.mods.includes(user.name))
            this.addUser(user, [user.name]);
    }

    whisperAll(user, params) {
        this.whisperHost(user, params, true);
        this.whisperMods(user, params, true);
    }

    whisperMods(user, params, all) {
        if (!this.enabled)
            return;

        let msg = `whisper from ${user.name}${all !== true ? ' to mods' : ''}: ${params.trim()}`;
        if (this.mods.length > 0)
            for (let mod of this.mods)
                this.api.sendNotice(
                    msg,
                    mod,
                    Constants.colors.dark_grey,
                    Constants.colors.light_grey,
                    'bold');
    }

    whisperHost(user, params, all) {
        if (!this.enabled)
            return;

        let msg = `whisper from ${user.name}${all !== true ? ' to you' : ''}: ${params.trim()}`;
        this.api.sendNotice(
            msg,
            this.api.room_slug,
            Constants.colors.dark_grey,
            all !== true ? Constants.colors.white : Constants.colors.light_grey,
            'bold');
        if (all !== true)
            this.api.sendNotice(
                `whispered to ${this.api.room_slug}: ${params.trim()}`,
                user.name,
                Constants.colors.dark_grey,
                Constants.colors.light_grey,
                'bold');
    }

    getStatus() {
        return `${this.name}: ${ this.enabled ? '[ running ]' : '[ stopped ]'} - Available commands:`;
    }
}

module.exports = Whispers;
