
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Legends extends Plugins {
    constructor() {
        super();
        this.name = 'Legends';

        this.settings = [
            {
                type: 'choice',
                name: 'legends_enable',
                label: 'Enable or disable legends',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            },
            {
                type: 'str',
                name: 'legends_header',
                label: 'Legends board header',
                defaultValue: 'Legends'
            },
            {
                type: 'str',
                name: 'legends_madlads',
                label: 'Madlads (comma separated)',
                defaultValue: ''
            },
            {
                type: 'int',
                name: 'legends_interval',
                label: 'Legends board interval (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 3
            },
            {
                type: 'choice',
                name: 'king_enable',
                label: 'Enable or disable crowning a king',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            },
            {
                type: 'str',
                name: 'legends_board_color',
                label: 'Legends board text color (hex)',
                defaultValue: Constants.colors.dark_blue
            },
            {
                type: 'str',
                name: 'legends_board_bg',
                label: 'Legends board background color (hex)',
                defaultValue: Constants.colors.light_purple
            },
            {
                type: 'str',
                name: 'legends_new_color',
                label: 'New legend text color (hex)',
                defaultValue: Constants.colors.dark_blue
            },
            {
                type: 'str',
                name: 'legends_new_bg',
                label: 'New legend background color (hex)',
                defaultValue: Constants.colors.light_blue
            },
            {
                type: 'str',
                name: 'legends_madlad_color',
                label: 'New madlad text color (hex)',
                defaultValue: Constants.colors.dark_purple
            },
            {
                type: 'str',
                name: 'legends_madlad_bg',
                label: 'New madlad background color (hex)',
                defaultValue: Constants.colors.light_purple
            },
            {
                type: 'str',
                name: 'legends_king_color',
                label: 'New king text color (hex)',
                defaultValue: Constants.colors.dark_purple
            },
            {
                type: 'str',
                name: 'legends_king_bg',
                label: 'New king background color (hex)',
                defaultValue: Constants.colors.cream
            }
        ];

        this.commands = {
            legends: {
                access: ['host', 'mod'],
                handler: this.legends,
                description: 'Do legendary stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Enable legends with "/legends start" or "/legends on"',
                        handler: this.enable,
                        success: 'Legends started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Disable legends with "/legends stop" or "/legends off"',
                        handler: this.disable,
                        success: 'Legends stopped.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'List legends with "/legends list", or "/legends list all" to display the legends board',
                        handler: this.showLegends,
                        success: 'Legends listed.'
                    },
                    {
                        names: ['add', 'new'],
                        description: 'Add a legend with "/legends add legendname"',
                        handler: this.addLegend,
                        success: 'Legend added.'
                    },
                    {
                        names: ['madlad', 'madd'],
                        description: 'Add a madlad with "/legends madlad madladname"',
                        handler: this.addMadlad,
                        success: 'Madlad added.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove a legend or madlad with "/legends remove username"',
                        handler: this.removeMadlad,
                        success: 'Madlad removed.'
                    },
                    {
                        names: ['ticket'],
                        description: 'Add legends to ticket show with "/legends ticket"',
                        handler: this.addLegends,
                        success: 'Legends added to ticket show.'
                    },
                    {
                        names: ['interval'],
                        description: 'Set legends board interval (in minutes, 0 to disable) with "/legends interval 3"',
                        handler: this.setInterval,
                        success: 'Legends board interval updated.'
                    }
                ]
            }
        };

        this.commands.legend = {
            access: ['host', 'mod'],
            handler: this.legends,
            description: 'Alias for /legends',
            params: /(.+)/,
            subcommands: this.commands.legends.subcommands
        };

        this.totals = {};
        this.king = '';
        this.king_amount = 500;

        this.enabled = true;
        this.interval = 3 * 60000;

        this.legends = [];
        this.madlads = [];
        this.separator = '|';
        this.legends_minimum = 1000;
        this.legends_madlads = 10000;
        this.header_icon = Constants.chars.text_queen;
        this.madlads_icon = Constants.chars.text_star;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    setInterval(user, params) {
        this.interval = Number.parseInt(params.join(''), 10) * 60000;
    }

    showInterval() {
        if (!this.enabled || this.interval === 0)
            return;
        this.showLegends();
        this.api.setTimeout(function(){ this.showInterval(); }.bind(this), this.interval);
    }

    showLegends(user, params) {
        let to = user ? user.name : '';
        let legends = [];

        let top = [];
        let header = this.api.settings.legends_header;
        top.push(this.header_icon + ' ' + header + ' ' + this.header_icon);
        top.push('-'.repeat(header.length + 9));

        for (let madlad of this.madlads)
            top.push(this.madlads_icon + ' ' + madlad + ' ' + this.madlads_icon);
        for (let legend of this.legends)
            legends.push(legend);

        for (let name of Object.keys(this.totals)) {
            let legend = name;
            if (this.madlads.includes(name) || this.legends.includes(name))
                continue;
            if (this.totals[name] >= this.legends_madlads)
                legend = this.madlads_icon + ' ' + legend + ' ' + this.madlads_icon;
            if (this.totals[name] >= this.legends_minimum)
                legends.push(legend);
        }
        legends.sort((a, b) => this.totals[a] - this.totals[b]).reverse();
        legends = top.concat(legends);
        legends.push('-'.repeat(header.length + 9));

        this.api.sendNotice(
            legends.join('\n'),
            (params && params.join('') === 'all') ? '' : to,
            this.api.settings.legends_board_bg,
            this.api.settings.legends_board_color,
            'bold'
        );
    }

    addLegend(user, params) {
        let legend = params.join('').trim();
        if (this.legends.includes(legend))
            return 'Legend already added.';
        this.legends.push(legend);
    }

    addMadlad(user, params) {
        let madlad = params.join('').trim();
        if (this.madlads.includes(madlad))
            return 'Madlad already added.';
        this.madlads.push(madlad);
    }

    removeMadlad(user, params) {
        let madlad = params.join('').trim();
        let index = this.madlads.indexOf(madlad);
        if (index < 0)
            return `${madlad} does not exist.`;
        this.madlads.splice(index, 1);
    }

    addLegends() {
        let legends = [];
        for (let name of Object.keys(this.totals))
            if (this.totals[name] >= this.legends_minimum && !this.api.limitCam_userHasAccess(name)) {
                legends.push(name);
                this.api.sendNotice(
                    `Welcome to the ticket show, legendary ${name}! (sent privately)`,
                    name,
                    this.api.settings.legends_new_bg,
                    this.api.settings.legends_new_color,
                    'bold');
            }
        this.api.limitCam_addUsers(legends);
    }

    onStart() {
        if (this.api.settings.legends_enable !== 'Enable')
            this.enabled = false;

        if (this.api.settings.legends_madlads) {
            let madlads = this.api.settings.legends_madlads.trim();
            if (madlads !== '')
                for (let madlad of madlads.split(','))
                    if (!this.madlads.includes(madlad))
                        this.madlads.push(madlad.trim());
        }

        if (['annahaven', 'canigetatest'].includes(this.api.room_slug)) {
            this.separator = ' ' + Constants.chars.text_dotted_line + ' ';
            this.header_icon = Constants.chars.text_queen_full;
            this.madlads_icon = Constants.chars.text_star_pointed;
        }

        this.interval = Number.parseInt(this.api.settings.legends_interval, 10) * 60000;
        if (this.interval > 0)
            this.showInterval();

        this.api.log('Legends started.');
    }

    onTip(from, to, amount, message) {
        if (from.isAnon)
            return;
        let last = 0;
        if (this.totals.hasOwnProperty(from.name)) {
            last = this.totals[from.name];
            this.totals[from.name] += amount;
        } else
            Object.assign(this.totals, {[from.name]: amount});

        if (this.enabled &&
            this.totals[from.name] >= this.legends_minimum &&
            last < this.legends_minimum)
            this.api.sendNotice(
              `Thank you ${from.name} for becoming a legend!`,
              '',
              this.api.settings.legends_new_bg,
              this.api.settings.legends_new_color,
              'bold');
        else if (this.enabled &&
                 this.totals[from.name] >= this.legends_madlads &&
                 last < this.legends_madlads)
                 this.api.sendNotice(
                   `Thank you ${from.name} for becoming a madlad!`,
                   '',
                   this.api.settings.legends_madlad_bg,
                   this.api.settings.legends_madlad_color,
                   'bold');

        if (this.api.settings.king_enable === 'Enable' &&
            this.totals[from.name] > this.king_amount) {
            this.king_amount = this.totals[from.name];
            if (from.name !== this.king) {
                this.king = from.name;
                this.api.sendNotice(
                    String.raw`:crowngold Congratulations ${from.name} on becoming the new king for the show!`,
                    '',
                    this.api.settings.legends_king_bg,
                    this.api.settings.legends_king_color,
                    'bold');
            }
        }
    }

    onMessage(user, message) {
        if (!this.totals.hasOwnProperty(user.name))
            Object.assign(this.totals, {[user.name]: 0});

        let total = this.totals[user.name];
        if (total > 0)
            message.setMessage(this.separator + total + this.separator + ' ' + message.message);

        if (this.enabled) {
            if (this.api.settings.king_enable === 'Enable' && user.name === this.king)
                message.setMessage(':crowngold ' + message.message);
            else if (total >= this.legends_madlads || this.madlads.includes(user.name))
                message.setMessage(Constants.chars.text_queen_full + ' ' + message.message);
            else if (total >= this.legends_minimum)
                message.setMessage(Constants.chars.text_queen + ' ' + message.message);
        }
    }

    getStatus() {
        return `${this.name}: ${ this.enabled ? '[ running ]' : '[ stopped ]'}` +
               ` Notice every ${this.interval / 60000} minute${this.interval > 1 ? 's' : ''} - Available commands:`;
    }
}

module.exports = Legends;
