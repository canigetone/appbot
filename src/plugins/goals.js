
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Goals extends Plugins {
    constructor() {
        super();
        this.name = 'Goals';
        this.total = 10;

        this.settings = [
            {
                type: 'choice',
                name: 'goals_enable',
                label: 'Enable or disable goals',
                choice1: 'Disable',
                choice2: 'Enable',
                defaultValue: 'Disable'
            },
            {
                type: 'choice',
                name: 'goals_next',
                label: 'Automatic next goal',
                choice1: 'Disable',
                choice2: 'Enable',
                defaultValue: 'Disable'
            },
            {
                type: 'choice',
                name: 'goals_type',
                label: 'Goal type',
                choice1: 'Tokens',
                choice2: 'Tippers',
                defaultValue: 'Tokens'
            },
            {
                type: 'int',
                name: 'goals_timer',
                label: 'Goal timer (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 0
            },
            {
                type: 'int',
                name: 'goals_interval',
                label: 'Display interval of notice (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 3
            },
            {
                type: 'str',
                name: 'goals_notice_color',
                label: 'Goal notice text color (hex)',
                defaultValue: Constants.colors.dark_green
            },
            {
                type: 'str',
                name: 'goals_notice_bg',
                label: 'Goal notice background color (hex)',
                defaultValue: Constants.colors.white
            },
            {
                type: 'str',
                name: 'goals_color',
                label: 'Goal reached text color (hex)',
                defaultValue: Constants.colors.dark_green
            },
            {
                type: 'str',
                name: 'goals_bg',
                label: 'Goal reached background color (hex)',
                defaultValue: Constants.colors.white
            }
        ];

        for (let i = 0; i < this.total; i++) {
            let goal = {
                type: 'str',
                name: 'goal_' + (i + 1),
                label: 'Goal #' + (i + 1),
                required: false,
                defaultValue: i === 0 ? '123, goal name' : ''
            };
            this.settings.push(goal);
        }

        this.commands = {
            goals: {
                access: ['host', 'mod'],
                handler: this.goals,
                description: 'Do goals stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Start goals with "/goals start" or "/goals on"',
                        handler: this.enable,
                        success: 'Goals started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Stop goals with "/goals stop" or "/goals off"',
                        handler: this.disable,
                        success: 'Goals stopped.'
                    },
                    {
                        names: ['next'],
                        description: 'Move to next goal with "/goals next"',
                        handler: this.nextGoal,
                        success: 'Goals moved to next.'
                    },
                    {
                        names: ['auto'],
                        description: 'Toggle automatic next goal with "/goals auto on" and "/goals auto off"',
                        handler: this.autoGoal,
                        success: 'Automatic next goal updated.'
                    },
                    {
                        names: ['timer'],
                        description: 'Edit goal timer with "/goals timer 15"',
                        handler: this.updateTimer,
                        success: 'Goal timer updated.'
                    },
                    {
                        names: ['type'],
                        description: 'Set goal type with "/goals type tokens", "/goals type tippers"',
                        handler: this.goalType,
                        success: 'Goal type updated.'
                    },
                    {
                        names: ['reset'],
                        description: 'Reset goals to default settings with "/goals reset"',
                        handler: this.initGoals,
                        success: 'Goals reset to default settings.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'List goals with "/goals list" or "/goals show"',
                        handler: this.showGoals,
                        success: 'Goals listed.'
                    },
                    {
                        names: ['set', 'update'],
                        description: 'Update a later goal with "/goals set 3 1999 hot dance"',
                        handler: this.updateGoal,
                        success: 'Goal updated.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove a goal with "/goals remove 2"',
                        handler: this.removeGoal,
                        success: 'Goal removed.'
                    },
                    {
                        names: ['add'],
                        description: 'Add goal with "/goals add 1999 dance"',
                        handler: this.addGoal,
                        success: 'Goal added.'
                    },
                    {
                        names: ['edit', 'change'],
                        description: 'Edit current goal with "/goals edit new dance"',
                        handler: this.editGoal,
                        success: 'Goal modified.'
                    },
                    {
                        names: ['remain', 'left'],
                        description: 'Edit current goal\'s remaining total with "/goals left 999"',
                        handler: this.remainingGoal,
                        success: 'Goal remaining modified.'
                    },
                    {
                        names: ['total'],
                        description: 'Edit current goal total with "/goals total 2999"',
                        handler: this.totalGoal,
                        success: 'Goal total modified.'
                    },
                    {
                        names: ['interval'],
                        description: 'Set notice interval in minutes with "/goals interval 3"',
                        handler: this.setInterval,
                        success: 'Goal notice interval set.'
                    }
                ]
            }
        };

        Object.assign(this.commands, {
            goal: {
                access: ['host', 'mod'],
                handler: this.goals,
                description: 'Alias for /goals',
                params: /(.+)/,
                subcommands: this.commands.goals.subcommands
            },
            subject: {
                access: ['host'],
                handler: this.subject,
                description: 'Change room subject',
                params: /(.+)/
            }
        });

        this.panel = {
            template: '3_rows_11_21_31',
            row1_value: 'Goal: chill',
            row2_value: 'Tokens left: 0',
            row3_value: 'Last: -'
        };

        this.next = 0;
        this.items = [];
        this.types = ['tokens', 'tippers'];
        this.timer = 0;
        this.time_left = 0;
        this.goals_timer = 0;

        this.current = '';
        this.current_type = 0;
        this.current_left = 0;
        this.current_total = 0;
        this.autonext = false;

        this.tippers = [];
        this.last_tipper = '-';

        this.enabled = true;
        this.interval = 3 * 60000;
    }

    isStopped() {
        return !this.enabled || this.current_left === 0 || this.current_total === 0 || this.current === '';
    }

    enable() {
        this.enabled = true;
        if (this.time_left > 0)
              this.timerInterval();
        this.api.drawPanel();
    }

    disable() {
        this.enabled = false;
        if (this.timer)
            this.api.cancelTimeout(this.timer);
        this.api.drawPanel();
    }

    setInterval(user, params) {
        this.interval = Number.parseInt(params.join(''), 10) * 60000;
    }

    noticeInterval() {
        if (this.isStopped())
            return;
        this.showGoal();
        this.api.setTimeout(function(){ this.noticeInterval(); }.bind(this), this.interval);
    }

    updateTimer(user, params) {
        let time = Number.parseInt(params.join(''), 10);
        this.goals_timer = time;
        this.time_left = time ? time + 1 : 0;
        if (this.enabled && this.time_left === 0)
            this.api.drawPanel();
        this.timerInterval();
    }

    timerInterval() {
        if (this.isStopped() || !this.time_left)
            return;

        this.time_left -= 1;

        if (this.time_left === 0) {
            this.api.sendNotice(
                Constants.chars.text_hourglass + ' Time\'s up! ' + Constants.chars.text_hourglass,
                '',
                Constants.colors.white,
                Constants.colors.dark_red,
                'bold');
            if (this.autonext)
                this.nextGoal();
        } else if (this.time_left <= 5)
            this.api.sendNotice(
                `${Constants.chars.text_hourglass_flowing} There is ${this.time_left} ` +
                `minute${this.time_left > 1 ? 's' : ''} left to the timer!`,
                '',
                Constants.colors.white,
                Constants.colors.dark_orange,
                'bold');

        this.api.drawPanel();
        if (this.timer)
            this.api.cancelTimeout(this.timer);
        this.timer = this.api.setTimeout(function(){ this.timerInterval(); }.bind(this), 60000);
    }

    initGoals() {
        let items = [];
        for (let i = 0; i < this.total; i++) {
            let item = this.api.settings['goal_' + (i + 1)];
            if (!item)
                continue;
            item = item.split(',');
            let total = Number.parseInt(item.shift().trim(), 10);
            let name = item.join(' ').trim();
            if (name && total) {
                items.push({name: name, total: total});
            }
        }
        this.items = items;
        if (this.items.length > 0) {
            this.current = this.items[0].name;
            this.current_total = this.items[0].total;
            this.current_left = this.current_total;
        }
        this.api.drawPanel();
    }

    showGoal(user) {
        let to = user ? user.name : '';
        let goal = `Goal: ${this.current} with ${this.current_left} ${this.types[this.current_type]}`;
        if (this.timer)
            goal += ` and ${Constants.chars.text_hourglass_flowing} ${this.time_left} minute${this.time_left > 1 ? 's' : ''}`;
        goal += ' left';

        this.api.sendNotice(
            goal,
            to,
            this.api.settings.goals_notice_bg,
            this.api.settings.goals_notice_color,
            'bold');
    }

    showGoals(user, params) {
        let goals = [];
        for (let i = 0; i < this.items.length; i++)
            goals.push('Goal #' + (i + 1) + ' - ' + this.items[i].name +
                       ' @ ' + this.items[i].total + ' ' + this.types[this.current_type]);
        if (this.current_type === 1)
            goals.push('Tippers: ' + this.tippers.join(', '));

        this.api.sendNotice(
            goals.join('\n'),
            user.name,
            Constants.colors.white,
            Constants.colors.dark_green,
            'bold');
    }

    autoGoal(user, params) {
        this.autonext = params.join('').toLowerCase() === 'on';
    }

    goalType(user, params) {
        if (params.join('').toLowerCase() === 'tippers') {
            if (this.current_type === 0)
                this.tippers = [];
            this.current_type = 1;
        } else
            this.current_type = 0;
        this.api.drawPanel();
    }

    updateGoal(user, params) {
        let index = Number.parseInt(params.shift(), 10);
        let total = Number.parseInt(params.shift(), 10);
        let name = params.join(' ');
        let help = ', list goals with "/goal list" then ' +
                   'update a goal with "/goal set 2 3999 hot dance"';
        if (index > this.items.length)
            return `Goal #${index} does not exist` + help;
        if (Number.isNaN(total))
            return 'Invalid total' + help;
        if (index === 1)
            this.current = name;
        this.items[index - 1].name = name;
        this.items[index - 1].total = total;
        if (this.enabled)
            this.api.drawPanel();
    }

    addGoal(user, params) {
        let total = Number.parseInt(params.shift(), 10);
        let name = params.join(' ');
        let help = ', add a goal with "/goal add 1999 dance"';
        if (Number.isNaN(total))
            return 'Invalid total' + help;
        else if (name === '')
            return 'Invalid goal name' + help;
        this.items.push({name: name, total: total});
        if (this.items.length === 1)
            this.nextGoal(user);
    }

    editGoal(user, params) {
        let name = params.join(' ');
        this.current = name;
        if (this.items.length > 0)
            this.items[0].name = name;
        if (this.enabled)
            this.api.drawPanel();
    }

    totalGoal(user, params) {
        let total = Number.parseInt(params.shift(), 10);
        if (Number.isNaN(total))
            return 'Invalid amount, set it with /goal total 1999';
        this.current_total = total;
        if (this.items.length > 0)
            this.items[0].total = total;
        if (total < this.current_left)
            this.current_left = total;
        if (this.enabled)
            this.api.drawPanel();
    }

    remainingGoal(user, params) {
        let total = Number.parseInt(params.shift(), 10);
        if (Number.isNaN(total))
            return 'Invalid amount left, set it with /goal left 1499';
        this.current_left = total;
        if (this.enabled)
            this.api.drawPanel();
    }

    nextGoal(user) {
        if (this.items.length < 1)
            return 'No goal left, add a goal with "/goal add 1999 dance"';
        else if (this.items.length > 1)
            this.items.shift();
        this.current = this.items[0].name;
        this.current_total = this.items[0].total;
        this.current_left = this.current_total;
        if (this.goals_timer) {
            this.time_left = this.goals_timer + 1;
            this.timerInterval();
        }
        if (this.enabled) {
            this.showGoal();
            this.api.drawPanel();
        }
    }

    removeGoal(user, params) {
        let index = Number.parseInt(params, 10);
        if (Number.isNaN(index) || index > this.items.length)
            return `Goal #${index} does not exist.`;
        this.items.splice(index - 1, 1);
    }

    onStart() {
        if (this.api.settings.goals_enable !== 'Enable')
            this.enabled = false;

        this.initGoals();

        if (this.api.settings.goals_next === 'Enable')
            this.autonext = true;

        if (this.api.settings.goals_type === 'Tippers')
            this.current_type = 1;

        let goals_timer = Number.parseInt(this.api.settings.goals_timer, 10);
        if (goals_timer > 0) {
            this.goals_timer = goals_timer;
            this.time_left = goals_timer + 1;
            if (!this.isStopped())
                this.timerInterval();
        }

        this.interval = Number.parseInt(this.api.settings.goals_interval, 10) * 60000;
        if (this.interval > 0)
            this.noticeInterval();

        this.api.log('Goals started.');
    }

    onTip(from, to, amount, message) {
        if (this.isStopped())
            return;
        let tip_amount = amount;
        if (this.current_type === 1) {
            amount = 1;
            if (this.tippers.includes(from.name))
                return;
            else
                this.tippers.push(from.name);
        }
        if (amount >= this.current_left) {
            this.api.sendNotice(
                `Goal reached! ${this.current} will start soon.`,
                '',
                this.api.settings.goals_bg,
                this.api.settings.goals_color,
                'bold');

            if (this.autonext && this.items.length > 1) {
                let last_left = this.current_left;
                this.nextGoal();
                this.current_left = this.current_total - (amount - last_left);
            } else {
                this.current = this.current + ' (reached)';
                this.current_left = 0;
                this.time_left = 0;
                if (this.timer)
                    this.api.cancelTimeout(this.timer);
            }
        } else {
            this.current_left -= amount;
        }
        this.last_tipper = (from.isAnon ? 'Anonymous tip' : from.name) + ' (' + tip_amount + ')';
        this.api.drawPanel();
    }

    onEnter(user) {
        if (!this.enabled || this.current === '' || this.current_total === 0 || this.interval === 0)
            return;
        this.showGoal(user);
    }

    getStatus() {
        return `${this.name} (${this.types[this.current_type]}): ${ this.enabled ? '[ running ]' : '[ stopped ]'} ` +
               `Notice every ${this.interval / 60000} minute${this.interval > 1 ? 's' : ''} ` +
               `- Auto-next: ${this.autonext ? '[ on ]' : '[ off ]'} - Available commands:`;
    }

    subject(user, params) {
        this.api.changeRoomSubject(params.trim());
    }

    onDrawPanel(user, panel) {
        if (this.enabled && this.current !== '') {
            let current_type = 'Tokens';
            let current_left = this.current_left;
            let last = '';

            if (this.current_type === 1)
                current_type = 'Tippers';
            if (this.time_left)
                last = this.time_left + ' minute' + (this.time_left > 1 ? 's' : '') + ' left';

            panel.setPanel({
                template: '3_rows_11_21_31',
                row1_value: 'Goal: ' + this.current,
                row2_value: current_type + ' left: ' + current_left,
                row3_value: last
            });
            return panel.getResponse();
        }
    }
}

module.exports = Goals;
