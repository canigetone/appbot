
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Cleaner extends Plugins {
    constructor() {
        super();
        this.name = 'Cleaner';
        this.commands = {
            clean: {
                access: ['host', 'mod'],
                handler: this.clean,
                description: 'Clean up the chat with lines of dots...',
                params: /(\d+)/
            }
        };
    }

    clean(user, quantity) {
        let count;
        if (!quantity || !Number.parseInt(quantity, 10))
            quantity = 20;
        // this.api.log('Cleaning up...');
        let msg = [];
        for (count = 1; count <= quantity; count++)
            msg.push(".".repeat(count));
        this.api.sendNotice(msg.join('\n') + '\nMoving on.', '', Constants.colors.light_blue);
    }
}

module.exports = Cleaner;
