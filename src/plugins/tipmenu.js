
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class TipMenu extends Plugins {
    constructor() {
        super();
        this.name = 'Tip menu';
        this.total = 20;

        this.settings = [
            {
                type: 'choice',
                name: 'tipmenu_enable',
                label: 'Enable or disable the tip menu',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            },
            {
                type: 'choice',
                name: 'tipmenu_sort',
                label: 'Sort the tip menu',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Disable'
            },
            {
                type: 'int',
                name: 'tipmenu_interval',
                label: 'Display interval (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 5
            },
            {
                type: 'str',
                name: 'tipmenu_color',
                label: 'Tip menu text color (hex)',
                defaultValue: Constants.colors.dark_aqua
            },
            {
                type: 'str',
                name: 'tipmenu_bg',
                label: 'Tip menu background color (hex)',
                defaultValue: Constants.colors.light_aqua
            },
            {
                type: 'str',
                name: 'tipmenu_tip_color',
                label: 'New tip text color (hex)',
                defaultValue: Constants.colors.dark_purple
            },
            {
                type: 'str',
                name: 'tipmenu_tip_bg',
                label: 'New tip background color (hex)',
                defaultValue: Constants.colors.white
            },
            {
                type: 'str',
                name: 'tipmenu_prefix',
                label: 'Tip menu prefix',
                required: false
            }
        ];

        for (let i = 0; i < this.total; i++) {
            let item = {
                type: 'str',
                name: 'tipmenu_' + (i + 1),
                label: 'Item #' + (i + 1),
                required: false,
                defaultValue: i === 0 ? '12, tip item name' : ''
            };
            this.settings.push(item);
        }

        this.commands = {
            tipmenu: {
                handler: this.tipmenu,
                description: 'Show tip menu',
                params: /(.+)/
            },
            menu: {
                access: ['host', 'mod'],
                handler: this.menu,
                description: 'Do menu stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Start the tip menu with "/menu start" or "/menu on"',
                        handler: this.enable,
                        success: 'Tip menu started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Stop the tip menu with "/menu stop" or "/menu off"',
                        handler: this.disable,
                        success: 'Tip menu stopped.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'Alias to /tipmenu',
                        handler: this.showTipMenu,
                        success: 'Tip menu listed.'
                    },
                    {
                        names: ['sort'],
                        description: 'Sort the tip menu with "/menu sort"',
                        handler: this.sortTipMenu,
                        success: 'Tip menu sorted by price.'
                    },
                    {
                        names: ['unsort'],
                        description: 'Reset sorting of the tip menu with "/menu unsort"',
                        handler: this.unsortTipMenu,
                        success: 'Tip menu reset to default order.'
                    },
                    {
                        names: ['reset'],
                        description: 'Reset the tip menu to default settings with "/menu reset"',
                        handler: this.initTipMenu,
                        success: 'Tip menu reset to default settings.'
                    },
                    {
                        names: ['add', 'new'],
                        description: 'Add a tip menu item with "/menu add 123 New menu item"',
                        handler: this.addMenuItem,
                        success: 'Tip menu item added.'
                    },
                    {
                        names: ['edit', 'change'],
                        description: 'Edit tip menu item with "/menu edit 123 New menu item"',
                        handler: this.editMenuItem,
                        success: 'Tip menu item updated.'
                    },
                    {
                        names: ['price'],
                        description: 'Edit tip menu item price with "/menu price 123 234"',
                        handler: this.priceMenuItem,
                        success: 'Tip menu item price updated.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove tip menu item with "/menu remove 123"',
                        handler: this.removeMenuItem,
                        success: 'Tip menu item removed.'
                    },
                    {
                        names: ['interval'],
                        description: 'Set notice interval in minutes with "/menu interval 3"',
                        handler: this.setInterval,
                        success: 'Tip menu notice interval set.'
                    }
                ]
            }
        };

        this.keyed = {};
        this.items = [];
        this.prices = [];
        this.prefix = Constants.chars.text_star;

        this.enabled = true;
        this.interval = 5 * 60000;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    setInterval(user, params) {
        this.interval = Number.parseInt(params.join(''), 10) * 60000;
    }

    showInterval() {
        if (!this.enabled || this.interval === 0)
            return;
        this.showTipMenu();
        this.api.setTimeout(function(){ this.showInterval(); }.bind(this), this.interval);
    }

    initTipMenu() {
        let keyed = {};
        let items = [];
        let prices = [];

        for (let i = 0; i < this.total; i++) {
            let item = this.api.settings['tipmenu_' + (i + 1)];
            if (!item)
                continue;
            item = item.split(',');
            let price = Number.parseInt(item.shift().trim(), 10);
            let name = item.join(' ').trim();
            if (name && price) {
                items.push({name: name, price: price});
                Object.assign(keyed, {[price]: name});
                prices.push(price);
            }
        }

        this.keyed = keyed;
        this.items = items;
        this.prices = prices;
    }

    sortTipMenu() {
        this.items.sort((a, b) => a.price - b.price);
    }

    unsortTipMenu() {
        let items = [];
        for (let price of this.prices)
            items.push({name: this.keyed[price], price: price});
        this.items = items;
    }

    showTipMenu(user, params) {
        let menu = [];

        let to = user ? user.name : '';
        if (user && user.hasPermission(['host', 'mod']) && params && params.join('').trim() === 'all')
            to = '';

        for (let item of this.items)
            menu.push(this.prefix + ' ' + item.name + ' - ' + item.price + ' tokens');

        if (!this.enabled && user && user.hasPermission(['host', 'mod']))
            menu.push('Tip menu is currently disabled. You can enable it with "/menu on"');

        this.api.sendNotice(
            menu.join('\n'),
            to,
            this.api.settings.tipmenu_bg,
            this.api.settings.tipmenu_color,
            'bold');
    }

    addMenuItem(user, params) {
        let price = Number.parseInt(params.shift(), 10);
        let name = params.join(' ');

        if (this.prices.includes(price))
            return `Menu item already exists with a price of ${price} tokens. Edit or remove ${this.keyed[price]}`;

        Object.assign(this.keyed, {[price]: name});
        this.items.push({name: name, price: price});
        this.prices.push(price);
        if (this.api.settings.tipmenu_sort === 'Enable')
            this.sortTipMenu();
    }

    editMenuItem(user, params) {
        let price = Number.parseInt(params.shift(), 10);
        let name = params.join(' ');

        if (!this.prices.includes(price))
            return `Menu item does not exist with a price of ${price} tokens.`;

        let index = this.items.findIndex(i => i.price === price);
        if (index > -1)
            this.items[index].name = name;
        this.keyed[price] = name;
    }

    priceMenuItem(user, params) {
        let price = Number.parseInt(params.shift(), 10);
        let price_new = Number.parseInt(params, 10);

        if (!this.prices.includes(price))
            return `Menu item does not exist with a price of ${price} tokens.`;

        let index = this.items.findIndex(i => i.price === price);
        if (index > -1)
            this.items[index].price = price_new;

        index = this.prices.indexOf(price);
        if (index > -1) {
            Object.assign(this.keyed, {[price_new]: this.keyed[price]});
            this.prices[index] = price_new;
            delete this.keyed[price];
            if (this.api.settings.tipmenu_sort === 'Enable')
                this.sortTipMenu();
        }
    }

    removeMenuItem(user, params) {
        let price = Number.parseInt(params, 10);

        if (!this.prices.includes(price))
            return `Menu item does not exist with a price of ${price} tokens.`;

        let index = this.items.findIndex(i => i.price === price);
        if (index > -1)
            this.items.splice(index, 1);
        index = this.prices.indexOf(price);
        if (index > -1)
            this.prices.splice(index, 1);
        delete this.keyed[price];
    }

    onStart() {
        this.initTipMenu();

        if (this.api.settings.tipmenu_sort === 'Enable')
            this.sortTipMenu();

        if (this.api.settings.tipmenu_enable !== 'Enable')
            this.enabled = false;

        if (this.api.settings.tipmenu_prefix)
            this.prefix = this.api.settings.tipmenu_prefix;

        this.interval = Number.parseInt(this.api.settings.tipmenu_interval, 10) * 60000;
        if (this.interval > 0)
            this.showInterval();

        this.api.log('Tip menu has started.');
    }

    onTip(from, to, amount, message) {
        if (!this.enabled)
            return;
        if (this.prices.includes(Number.parseInt(amount, 10))) {
            this.api.sendNotice(
                (from.isAnon ? 'Anonymous' : from.name) + ` tipped for ${this.keyed[amount]}`,
                '',
                this.api.settings.tipmenu_tip_bg,
                this.api.settings.tipmenu_tip_color,
                'bold'
            );
        }
    }

    onEnter(user) {
        if (this.enabled)
            this.showTipMenu(user);
    }

    getStatus() {
        return `${this.name}: ${ this.enabled ? '[ running ]' : '[ stopped ]'}` +
               ` Notice every ${this.interval / 60000} minute${this.interval > 1 ? 's' : ''} - Available commands:`;
    }

    tipmenu(user, params) {
        if (this.enabled || user.hasPermission(['host', 'mod']))
            this.showTipMenu(user, params);
        else
            this.api.sendNotice('Tip menu is currently disabled.', user.name, Constants.colors.light_red);
    }
}

module.exports = TipMenu;
