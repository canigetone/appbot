
let Constants = require('chaturbate').constants;
let Plugins = require('chaturbate').pluginAbstract;

class Notices extends Plugins {
    constructor() {
        super();
        this.name = 'Notices';
        this.total = 10;

        this.settings = [
            {
                type: 'choice',
                name: 'notices_enable',
                label: 'Enable or disable notices',
                choice1: 'Enable',
                choice2: 'Disable',
                defaultValue: 'Enable'
            },
            {
                type: 'int',
                name: 'notices_interval',
                label: 'Display interval (in minutes, 0 to disable)',
                minValue: 0,
                maxValue: 120,
                defaultValue: 4
            },
            {
                type: 'str',
                name: 'notices_color',
                label: 'Notices text color (hex)',
                defaultValue: Constants.colors.dark_grey
            },
            {
                type: 'str',
                name: 'notices_bg',
                label: 'Notices background color (hex)',
                defaultValue: Constants.colors.white
            },
        ];

        for (let i = 0; i < this.total; i++) {
            let notice = {
                type: 'str',
                name: 'notice_' + (i + 1),
                label: 'Notice #' + (i + 1),
                required: false,
                defaultValue: ''
            };
            this.settings.push(notice);
        }

        this.commands = {
            notices: {
                access: ['host', 'mod'],
                handler: this.notices,
                description: 'Do notices stuff',
                params: /(.+)/,
                subcommands: [
                    {
                        names: ['start', 'on'],
                        description: 'Enable notices with "/notices start" or "/notices on"',
                        handler: this.enable,
                        success: 'Notices started.'
                    },
                    {
                        names: ['stop', 'off'],
                        description: 'Disable notices with "/notices stop" or "/notices off"',
                        handler: this.disable,
                        success: 'Notices stopped.'
                    },
                    {
                        names: ['reset'],
                        description: 'Reset notices to default settings with "/notices reset"',
                        handler: this.initNotices,
                        success: 'Notices reset to defaults.'
                    },
                    {
                        names: ['list', 'show'],
                        description: 'List notices with "/notices list", or "/notices list all" to display all notices publicly',
                        handler: this.showNotices,
                        success: 'Notices listed.'
                    },
                    {
                        names: ['add', 'new'],
                        description: 'Add a notice with "/notices add A new notice"',
                        handler: this.addNotice,
                        success: 'Notice added.'
                    },
                    {
                        names: ['edit', 'change'],
                        description: 'Edit a notice with "/notices edit 3 Edited notice"',
                        handler: this.editNotice,
                        success: 'Notice updated.'
                    },
                    {
                        names: ['remove', 'rm', 'del'],
                        description: 'Remove a notice with "/notices remove 3"',
                        handler: this.removeNotice,
                        success: 'Notice removed.'
                    },
                    {
                        names: ['interval'],
                        description: 'Set notice interval (in minutes, 0 to disable) with "/notices interval 3"',
                        handler: this.setInterval,
                        success: 'Notice interval updated.'
                    }
                ]
            }
        };

        this.commands.notice = {
            access: ['host', 'mod'],
            handler: this.notices,
            description: 'Alias for /notices',
            params: /(.+)/,
            subcommands: this.commands.notices.subcommands
        };

        this.items = [];
        this.next = 0;

        this.enabled = true;
        this.interval = 2 * 60000;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    setInterval(user, params) {
        this.interval = Number.parseInt(params.join(''), 10) * 60000;
    }

    showInterval() {
        if (!this.enabled || this.interval === 0)
            return;
        this.showNotice();
        this.api.setTimeout(function(){ this.showInterval(); }.bind(this), this.interval);
    }

    initNotices() {
        let items = [];
        for (let i = 0; i < this.total; i++) {
            let notice = this.api.settings['notice_' + (i + 1)];
            if (notice)
                items.push(notice.trim());
        }
        this.items = items;
    }

    showNotice() {
        if (!this.items)
            return;
        let notice = this.items[this.next];
        this.next = this.next < this.items.length - 1 ? this.next + 1 : 0;
        this.api.sendNotice(
            notice,
            '',
            this.api.settings.notices_bg,
            this.api.settings.notices_color,
            'bold'
        );
    }

    showNotices(user, params) {
        let to = user ? user.name : '';
        if (user && user.hasPermission(['host', 'mod']) && params && params.join('').trim() === 'all')
            to = '';

        let notices = [];
        for (let i = 0; i < this.items.length; i++)
            notices.push('#' + (i + 1) + ' - ' + this.items[i]);

        this.api.sendNotice(
            notices.join('\n'),
            to,
            this.api.settings.notices_bg,
            this.api.settings.notices_color,
            'bold'
        );
    }

    addNotice(user, params) {
        let notice = params.join(' ').trim();
        if (this.items.includes(notice))
            return 'This notice already exists.';
        this.items.push(notice);
    }

    editNotice(user, params) {
        let index = Number.parseInt(params.shift(), 10);
        if (index > this.items.length)
            return `Notice #${index} does not exist.`;
        let notice = params.join(' ');
        this.items[index - 1] = notice;
    }

    removeNotice(user, params) {
        let index = Number.parseInt(params, 10);
        if (index > this.items.length)
            return `Notice #${index} does not exist.`;
        this.items.splice(index - 1, 1);
    }

    onStart() {
        this.initNotices();

        if (this.api.settings.notices_enable !== 'Enable')
            this.enabled = false;

        this.interval = Number.parseInt(this.api.settings.notices_interval, 10) * 60000;
        if (this.interval > 0)
            this.showInterval();

        this.api.log('Notices started.');
    }

    getStatus() {
        return `${this.name}: ${ this.enabled ? '[ running ]' : '[ stopped ]'}` +
               ` Notice every ${this.interval / 60000} minute${this.interval > 1 ? 's' : ''} - Available commands:`;
    }
}

module.exports = Notices;
