// canigetabot

let Bot = require('chaturbate').bot,
    Blocker = require('./plugins/blocker'),
    Cleaner = require('./plugins/cleaner'),
    Notices = require('./plugins/notices'),
    Legends = require('./plugins/legends'),
    Tipmenu = require('./plugins/tipmenu'),
    Whispers = require('./plugins/whispers');

let bot = new Bot(cb);
bot.register(new Notices());
bot.register(new Tipmenu());
bot.register(new Legends());
bot.register(new Cleaner());
bot.register(new Blocker());
bot.register(new Whispers());
bot.run();
