
module.exports = {
  mode: 'production',
  entry: {
    app: './src/app.js',
    bot: './src/bot.js'
  },
  externals: {
    cb: 'cb',
    cbjs: 'cbjs'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {},
      },
    ],
  }
};
