Anna Haven's App & Bot
======================

Requirements
------------
NodeJS and npm

Installation
------------
Clone this repository alongside `chaturbate` framework:
```
git clone https://gitlab.com/canigetone/chaturbate
git clone https://gitlab.com/canigetone/appbot
```

Building
-----
```
cd appbot
npm run build
```
Builds `./dist/app.js` and `./dist/bot.js`
